# About sample project

This sample project demonstrates REST API testing using Java, Serenity and Cucumber frameworks.

## Prerequisites
* java openjdk 11
* maven

## Executing the tests

To run all tests, go to the root where pom.xml file is placed and run from command line:

```diff
mvn clean verify -Denvironment=dev
```

Alternatively, you can run only tests tagged with annotanios, for example '@type=smoke':

```diff
mvn clean verify -Denvironment=qa -Dcucumber.options="--tags @type=smoke"
```

The test results will be saved in the target/site/serenity/index.html.

![Test Results](docs/test-result.png)

For every request and response, there is a log in report. Click on button query to see it.

![Test Results](docs/querry.png)


