package starter;

public enum WebServiceEndPoints {
    STORE_ORDER("/v2/store/order"),
    STORE_INVENTORY("/v2/store/inventory"),
    GET_ORDER_BY_ID("/v2/store/order/{orderId}"),
    DELETE_ORDER_BY_ID("/v2/store/order/{orderId}"),
    CREATE_PET("/v2/pet"),
    GET_PET_BY_ID("/v2/pet/{id}"),
    UPDATE_PET("/v2/pet"),
    DELETE_PET("/v2/pet/{id}"),
    CREATE_USER("/v2/user"),
    GET_USER_BY_USERNAME("/v2/user/{username}"),
    DELETE_USER_BY_USERNAME("/v2/user/{username}"),
    UPDATE_USER_BY_USERNAME("/v2/user/{username}"),
    CREATE_LIST_OF_USERS("/v2/user/createWithList");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
