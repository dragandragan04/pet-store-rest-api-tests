package starter.exceptions;

import net.serenitybdd.core.exceptions.TestCompromisedException;

public class IncorrectConfigurationPropertyException extends TestCompromisedException {

    public static final String MESSAGE = "Your configuration property cannot be blank";

    public IncorrectConfigurationPropertyException() {
        super(MESSAGE);
    }
}
