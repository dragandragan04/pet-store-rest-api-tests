package starter.exceptions;

import net.serenitybdd.core.exceptions.TestCompromisedException;

public class UndefinedConfigurationProperty extends TestCompromisedException {

    public UndefinedConfigurationProperty(Throwable cause) {
        super(cause);
    }
}
