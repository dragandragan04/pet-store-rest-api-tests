package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.WebServiceEndPoints;
import starter.templates.FieldValues;
import starter.templates.MergeFrom;
import starter.services.RestApiService;
import starter.services.RestApiResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;

public class StoreStepDefinitions {

    @Steps
    RestApiResponse responseDetails;

    @Steps
    RestApiService recordNewTrade;

    String order;

    @When("user ask for pet inventories by status")
    public void first_Call() {
        recordNewTrade.find(WebServiceEndPoints.STORE_INVENTORY.getUrl());
    }

    @Given("the following order")
    public void placeOrderPayload(List<Map<String, String>> orderDetails) throws IOException {
        order = MergeFrom.template("templates/store/order.json")
                .withDefaultValuesFrom(FieldValues.in("templates/store/standard-order.properties"))
                .withFieldsFrom(orderDetails.get(0));
    }

    @When("user want to find order by id {string}")
    public void user_wants_to_find_order_by_id(String orderId) {
        Map map = new HashMap();
        map.put("orderId", orderId);
        recordNewTrade.find(WebServiceEndPoints.GET_ORDER_BY_ID.getUrl() ,map);
    }

    @When("user want to delete order by id {string}")
    public void user_wants_to_delete_order_by_id(String orderId) {
        Map map = new HashMap();
        map.put("orderId", orderId);
        recordNewTrade.delete(WebServiceEndPoints.DELETE_ORDER_BY_ID.getUrl() ,map);
    }

    @When("user create the order")
    public void user_record_the_trade() {
        recordNewTrade.postRequestWithDetails(order, WebServiceEndPoints.STORE_ORDER.getUrl());
    }

    @Then("response status should be {string}")
    public void response_status_should_be(String responseStatusCode) {
        restAssuredThat(response -> response.statusCode(Integer.valueOf(responseStatusCode)));
    }

    @Then("response should contain following details")
    public void response_should_contain_following_details(List<Map<String, String>> tradeDetails) {

        Map<String, String> expectedResponse = tradeDetails.get(0);
        Map<String, String> actualResponse = responseDetails.returned();

        assertThat(actualResponse).containsAllEntriesOf(expectedResponse);
    }
}
