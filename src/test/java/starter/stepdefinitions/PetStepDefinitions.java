package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.WebServiceEndPoints;
import starter.templates.FieldValues;
import starter.templates.MergeFrom;
import starter.services.RestApiService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PetStepDefinitions {

    @Steps
    RestApiService restApiService;

    String pet;

    @Given("pet object that needs to be added to the store")
    public void pet_object_that_needs_to_be_added_to_the_store(List<Map<String, String>> orderDetails) throws IOException {
        pet = MergeFrom.template("templates/pet/pet.json")
                .withDefaultValuesFrom(FieldValues.in("templates/pet/default-pet.properties"))
                .withFieldsFrom(orderDetails.get(0));
    }

    @When("user create pet")
    public void user_create_pet() {
        restApiService.postRequestWithDetails(pet, WebServiceEndPoints.CREATE_PET.getUrl());
    }

    @When("user search for pet with id {string}")
    public void user_search_for_pet_with_id(String petId) {
        Map map = new HashMap();
        map.put("id", petId);
        restApiService.find(WebServiceEndPoints.GET_PET_BY_ID.getUrl(), map);
    }

    @When("user update following pet properties")
    public void user_create_pet(List<Map<String, String>> orderDetails) throws IOException {
        pet = MergeFrom.template("templates/pet/pet.json")
                .withDefaultValuesFrom(FieldValues.in("templates/pet/default-pet.properties"))
                .withFieldsFrom(orderDetails.get(0));
        restApiService.update(pet, WebServiceEndPoints.UPDATE_PET.getUrl());
    }

    @When("user delete pet with id {string}")
    public void user_delete_pet_with_id(String id) {
        Map map = new HashMap();
        map.put("id", id);
        restApiService.delete(WebServiceEndPoints.DELETE_PET.getUrl(), map);
    }
}
