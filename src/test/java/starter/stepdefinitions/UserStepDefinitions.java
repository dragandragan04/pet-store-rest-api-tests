package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.WebServiceEndPoints;
import starter.templates.FieldValues;
import starter.templates.MergeFrom;
import starter.services.RestApiService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserStepDefinitions {

    @Steps
    RestApiService restApiService;

    String user;

    @Given("user that needs to be created")
    public void user_that_needs_to_be_created(List<Map<String, String>> userDetails) throws IOException {
        user = MergeFrom.template("templates/user/user.json")
                .withDefaultValuesFrom(FieldValues.in("templates/user/default-user.properties"))
                .withFieldsFrom(userDetails.get(0));
    }

    @Given("list of users to be created")
    public void list_of_users_to_be_created(List<Map<String, String>> userDetails) throws IOException {
        user = MergeFrom.template("templates/user/user-list.json")
                .withDefaultValuesFrom(FieldValues.in("templates/user/default-user.properties"))
                .withFieldsFrom(userDetails.get(0));
    }

    @When("I create user")
    public void i_create_user() {
        restApiService.postRequestWithDetails(user, WebServiceEndPoints.CREATE_USER.getUrl());
    }

    @When("I create user list of users")
    public void i_create_list_of_users() {
        restApiService.postRequestWithDetails(user, WebServiceEndPoints.CREATE_LIST_OF_USERS.getUrl());
    }

    @When("I search for user with username {string}")
    public void i_search_for_user_with_username(String username) {
        Map map = new HashMap();
        map.put("username", username);
        restApiService.find(WebServiceEndPoints.GET_USER_BY_USERNAME.getUrl(), map);
    }

    @When("I delete user with username {string}")
    public void i_delete_user_with_username(String username) {
        Map map = new HashMap();
        map.put("username", username);
        restApiService.delete(WebServiceEndPoints.DELETE_USER_BY_USERNAME.getUrl(), map);
    }

    @When("I update following user properties for user with username {string}")
    public void i_update_following_user_properties(String username, List<Map<String, String>> userDetails) throws IOException {
        user = MergeFrom.template("templates/user/user.json")
                .withDefaultValuesFrom(FieldValues.in("templates/user/default-user.properties"))
                .withFieldsFrom(userDetails.get(0));

        Map map = new HashMap();
        map.put("username", username);
        restApiService.update(user, WebServiceEndPoints.UPDATE_USER_BY_USERNAME.getUrl(), map);
    }

}
