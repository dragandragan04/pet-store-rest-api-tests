package starter.services;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import starter.config.EnvConfig;

import java.util.Map;

public class RestApiService {

    String baseUrl = EnvConfig.fromConfigFile().getBaseUrl();

    @Step("Post request with details")
    public void postRequestWithDetails(String body, String endPoint) {

        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .when().log().all()
                .post(endPoint)
                .then().log().all();
    }

    @Step("Get request")
    public void find(String endPoint) {
        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .when()
                .get(endPoint)
                .then().log().all();
    }

    @Step("Get request with path parameter")
    public void find(String endPoint, Map<String, String> pathParameter) {
        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .pathParams(pathParameter)
                .when()
                .get(endPoint)
                .then().log().all();
    }

    @Step("Delete request with path parameter")
    public void delete(String endPoint, Map<String, String> pathParameter) {
        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .pathParams(pathParameter)
                .when()
                .delete(endPoint)
                .then().log().all();
    }

    @Step("Update request")
    public void update(String body, String endPoint) {
        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .put(endPoint)
                .then().log().all();
    }

    @Step("Update request with path parameters")
    public void update(String body, String endPoint, Map<String, String> pathParameter) {
        SerenityRest.given()
                .baseUri(baseUrl)
                .log().all()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .pathParams(pathParameter)
                .body(body)
                .when()
                .put(endPoint)
                .then().log().all();
    }
}
