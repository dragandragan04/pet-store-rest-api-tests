package starter.config;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.environment.UndefinedEnvironmentVariableException;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import starter.exceptions.IncorrectConfigurationPropertyException;
import starter.exceptions.UndefinedConfigurationProperty;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class EnvConfig {

    private final EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

    private String baseUrl;

    public static EnvConfig fromConfigFile() {
        return new EnvConfig();
    }

    public EnvConfig() {
        try {
            baseUrl = EnvironmentSpecificConfiguration.from(variables).getProperty("base.url");
        }catch(UndefinedEnvironmentVariableException e){
            throw new UndefinedConfigurationProperty(e);
        }

        if (isBlank(baseUrl)) {
            throw new IncorrectConfigurationPropertyException();
        }
    }

    public String getBaseUrl() {
        return baseUrl;
    }

}
