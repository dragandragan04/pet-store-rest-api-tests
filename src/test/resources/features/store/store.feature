Feature: Petstore orders


  Scenario: Verify that user can place an order for a pet
    Given the following order
      | id | petId | quantity | shipDate                  | status |
      | 0  | 0     | 0        | 2020-12-01T13:26:38.013Z  | placed |
    When user create the order
    Then response status should be "200"
    And response should contain following details
      | shipDate                      | status | complete |
      | 2020-12-01T13:26:38.013+0000  | placed | true     |

  Scenario: For invalid ship date user should get response status 400
    Given the following order
      | id | petId | quantity | shipDate                  | status |
      | 0  | 0     | 0        | dsfdsfdsfdsfsdfsdfsdfsdf  | placed |
    When user create the order
    Then response status should be "400"

  Scenario: For invalid pet ID system should response with status code 400
    Given the following order
      | id | petId | quantity | shipDate                  | status |
      | 0  | sdfsd | 0        | dsfdsfdsfdsfsdfsdfsdfsdf  | placed |
    When user create the order
    Then response status should be "400"
    And response should contain following details
      | code | type    | message   |
      | 400  | unknown | bad input |

  Scenario: Verify if Get pet inventories by status works
    When user ask for pet inventories by status
    Then response status should be "200"


  Scenario: Get order by id should return correct response
    Given the following order
      | id | petId | quantity | shipDate                  | status |
      | 1  | 1     | 1        | 2020-12-01T13:26:38.013Z  | placed |
    When user create the order
    When user want to find order by id "1"
    Then response status should be "200"
    And response should contain following details
      | id | petId | status |
      | 1  | 1     | placed |

  Scenario: Get order by id should return response 404 when id does not exist
    When user want to find order by id "11111111111"
    Then response status should be "404"
    And response should contain following details
      | code | type  | message         |
      | 1    | error | Order not found |

  Scenario: Delete order by id should return response 404 when id does not exist
    When user want to delete order by id "11111111111"
    Then response status should be "404"
    And response should contain following details
      | code | type    | message         |
      | 404  | unknown | Order Not Found |