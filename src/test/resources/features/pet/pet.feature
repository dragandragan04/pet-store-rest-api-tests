Feature: Everything about your Pets

  Scenario: Add a new pet to the store
    Given pet object that needs to be added to the store
      | id    | categoryName | name   | status    |
      | 21221 | Fish         | Monika | available |
    When user create pet
    Then response status should be "200"
    And response should contain following details
      |           tags           | status    | name   |
      | [{id=11132, name=black}] | available | Monika |
    When user search for pet with id '21221'
    Then response should contain following details
      | id    |           tags           | status    | name   | category             | photoUrls        |
      | 21221 | [{id=11132, name=black}] | available | Monika | {id=1188, name=Fish} | [www.google.com] |

  @type=smoke
  Scenario: Pet cannot be created when id is invalid
    Given pet object that needs to be added to the store
      | id    |
      | ssfsd |
    When user create pet
    Then response status should be "400"

  Scenario: Response 404 when user search for not existing pet ID,
    When user search for pet with id '-1'
    Then response status should be "404"
    And response should contain following details
      | code | type  | message       |
      | 1    | error | Pet not found |


  Scenario: Update pet happy flow
    Given pet object that needs to be added to the store
      | id    | categoryName | name  |
      | 21228 | Mouse        | Jerry |
    When user create pet
    Then response status should be "200"
    When user update following pet properties
      | id    | categoryName | name   |
      | 21228 | penguin      | Mumble |
    When user search for pet with id '21228'
    Then response should contain following details
      | id    |           tags           | status    | name   | category                | photoUrls        |
      | 21228 | [{id=11132, name=black}] | available | Mumble | {id=1188, name=penguin} | [www.google.com] |

  Scenario: Update pet when invalid ID is suplied
    When user update following pet properties
      | id    | categoryName | name   |
      | ddddd | penguin      | Mumble |
    Then response status should be "400"
    And response should contain following details
      | code | type    | message   |
      | 400  | unknown | bad input |

  Scenario: Delete a pet
    Given pet object that needs to be added to the store
      | id      | categoryName | name   | status    |
      | 2122131 | Fish         | Monika | available |
    When user create pet
    Then response status should be "200"
    When user delete pet with id "2122131"
    Then response status should be "200"
    And response should contain following details
      | code | type    | message |
      | 200  | unknown | 2122131 |
    When user delete pet with id "2122131"
    Then response status should be "404"