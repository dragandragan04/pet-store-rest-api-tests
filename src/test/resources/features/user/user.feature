Feature: Operations about user

  @smoke
  Scenario: Validate adding a new user
    Given user that needs to be created
      | id     | username | firstName  | lastName  | email          |
      | 212281 | user1    | firstName1 | lastName1 | user1@mail.com |
    When I create user
    Then response status should be "200"
    And response should contain following details
      | code | type    | message |
      | 200  | unknown | 212281  |
    When I search for user with username "user1"
    Then response status should be "200"
    And response should contain following details
      | username | email          |
      | user1    | user1@mail.com |

  Scenario: User not found response when I search for not existing username
    When I search for user with username "dfsdfsdfsdfsdfsd"
    Then response status should be "404"
    And response should contain following details
      | code | type  | message        |
      | 1    | error | User not found |

  Scenario: Validate deleting user
    Given user that needs to be created
      | id     | username     | firstName  | lastName  | email                 |
      | 333333 | userToDelete | firstName1 | lastName1 | userToDelete@mail.com |
    When I create user
    Then response status should be "200"
    When I delete user with username "userToDelete"
    Then response status should be "200"
    And response should contain following details
      | code | type    | message      |
      | 200  | unknown | userToDelete |
    When I delete user with username "userToDelete"
    Then response status should be "404"

  Scenario: Validate updating user
    Given user that needs to be created
      | id     | username     | firstName  | lastName  | email            |
      | 654666 | userToUpdate | firstName1 | lastName1 | user654@mail.com |
    When I create user
    Then response status should be "200"
    When I update following user properties for user with username "userToUpdate"
      | id     | username     | firstName    | lastName    | email             |
      | 654666 | userToUpdate | newFirstName | newLastName | newEmail@mail.com |
    When I search for user with username "userToUpdate"
    Then response status should be "200"
    And response should contain following details
      | username     | firstName    | lastName    | email             |
      | userToUpdate | newFirstName | newLastName | newEmail@mail.com |

    Scenario: Validate creating list of users
      Given list of users to be created
        | userId1  | userId2  | userName1     | userName2      |
        | 21312322 | 52255755 | firstUserNam  | secondUserNam  |
      When I create user list of users
      Then response status should be "200"
      When I search for user with username "firstUserNam"
      Then response status should be "200"
      When I search for user with username "secondUserNam"
      Then response status should be "200"
